package com.srini.kafkaconfluentconsumer1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * The type Kafka confluent consumer 1 application.
 */
@SpringBootApplication
@EnableKafka
public class KafkaConfluentConsumer1Application {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(KafkaConfluentConsumer1Application.class, args);
	}

}
