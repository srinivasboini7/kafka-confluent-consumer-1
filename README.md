# kafka-confluent-consumer-1


Use below VM arguments while running the app.

-DCLUSTER_API_KEY=<your cluster API Key from confluent>
-DCLUSTER_API_SECRET=<your cluster API Secret from confluent>
-DSR_API_KEY=<your schema registry API Key>
-DSR_API_SECRET=<your schema registry API Secret>